// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/* ========================================================
 * |             15-640 Distributed System                |
 * |                   Project 1: RPC                     |
 * ========================================================
 *
 * mylib.so
 *
 * This library is used to intercept normal file operation calls
 *     and replace with RPC towards server
 * Intercepted File Operations include:
 *     - Open
 *     - Close
 *     - Read
 *     - Write
 *     - Lseek
 *     - Stat(__xstat)
 *     - Unlink
 *     - Getdirentries
 * And two user defined file operation from dirtree.so:
 *     - Getdirtree
 *     - Freedirtree
 *
 * All common procedure related to message marshal/unmarshal/send/recv are
 *     implemented in message_helper module
 * This file would mainly focus on fill in actual data fields into allocated
 *     location in the message packet
 */

#define _GNU_SOURCE

#include "message_helper.h"

// used to differentiate fd assigned by RPC and fd used by local client
#define SERVER_FD_OFFSET 100000

// global socket fd variable
static int client_sockfd = 0;


/* @summary: function to init network communication
 * @detail: use environment variable to determine server ip and port
 *          connect to server once library is loaded by caller program
 */
int init_connection() {
    char* serverip = NULL;
    char* serverport = NULL;
    unsigned short port;
    struct sockaddr_in srv;
    int rv;

    // read server ip/port
    serverip = getenv("server15440");
    if (serverip == NULL) {
        serverip = "127.0.0.1";
    }

    serverport = getenv("serverport15440");
    if (serverport) {
        port = (unsigned short)atoi(serverport);
    } else {
        port = 15987;
    }

    // create socket
    client_sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (client_sockfd < 0) {
        print_error("Network Error", "Cannot create socket");\
        return -1;
    }

    // setup address structure
    memset(&srv, 0, sizeof(srv));
    srv.sin_family = AF_INET;
    srv.sin_addr.s_addr = inet_addr(serverip);
    srv.sin_port = htons(port);

    // connect to server
    rv = connect(client_sockfd, (struct sockaddr*)&srv,
                 sizeof(struct sockaddr));
    if (rv < 0) {
        print_error("Network Error", "Cannot connect to server");
        return -1;
    }

    return 0;
}


/* following lines declare function pointers for original c lib calls
 * the original fps are used for:
 *     - testing in checkpoint1
 *     - pass portion of calls with local fd to original fop calls
 */
int (*orig_open)(const char *pathname, int flags, ...);
int (*orig_close)(int fd);
ssize_t (*orig_read)(int fd, void* buf, size_t nbyte);
ssize_t (*orig_write)(int fd, const void* buf, size_t count);
off_t (*orig_lseek)(int fd, off_t offset, int whence);
int (*orig_xstat)(int vers, const char* path, struct stat* buf);
int (*orig_unlink)(const char* pathname);
ssize_t (*orig_getdirentries)(int fd, char* buf, size_t nbytes, off_t* basep);
struct dirtreenode* (*orig_getdirtree)(const char* path);
void (*orig_freedirtree)(struct dirtreenode* dt);



/** ! START OF INTERCEPTED CALLS ! **/

int open(const char *pathname, int flags, ...) {
	mode_t m=0;
	if (flags & O_CREAT) {
		va_list a;
		va_start(a, flags);
		m = va_arg(a, mode_t);
		va_end(a);
	}

    // init packet structure
    int path_len = strlen(pathname);
    int packet_len = sizeof(PackedStream) + sizeof(OpenStruct) +
                     path_len + 1;
    OpenStruct* args;
    PackedStream* msg = prepare_msg(OP_OPEN, (void**)(&args), packet_len);

    // fill in argument struct
    args->flags = flags;
    args->m = m;
    strcpy(args->path, pathname);

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    OpenStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    int fd = retArgs->fd;
    if (fd == -1) {
        errno = retMsg->opcode;
    }

    // clean up receive buffer
    free(retMsg);

    if (fd != -1)
        return fd + SERVER_FD_OFFSET;
    else
        return fd;
}


int close(int fd) {
    // check fd validity
    if (fd < SERVER_FD_OFFSET) {
        return orig_close(fd);
    }

    fd -= SERVER_FD_OFFSET;

    // init packet structure
    int packet_len = sizeof(PackedStream) + sizeof(CloseStruct);
    CloseStruct* args;
    PackedStream* msg = prepare_msg(OP_CLOSE, (void**)(&args), packet_len);

    // fill in argument struct
    args->fd = fd;

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    CloseStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    int ret_val = retArgs->ret_val;
    if (ret_val == -1) {
        errno = retMsg->opcode;
    }

    // clean up receive buffer
    free(retMsg);

	return ret_val;
}


ssize_t read(int fd, void* buf, size_t nbyte) {
    // check fd validity
    if (fd < SERVER_FD_OFFSET) {
        return orig_read(fd, buf, nbyte);
    }

    fd -= SERVER_FD_OFFSET;

    // init packet structure
    int packet_len = sizeof(PackedStream) + sizeof(ReadStruct);
    ReadStruct* args;
    PackedStream* msg = prepare_msg(OP_READ, (void**)(&args), packet_len);

    // fill in argument struct
    args->fd = fd;
    args->count = nbyte;

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    ReadStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    ssize_t ret_count = retArgs->ret_count;
    if (ret_count > 0)
        memcpy(buf, retArgs->buf, ret_count);
    if (ret_count == -1)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return ret_count;
}


ssize_t write(int fd, const void* buf, size_t count) {
    // check fd validity
    if (fd < SERVER_FD_OFFSET) {
        return orig_write(fd, buf, count);
    }

    fd -= SERVER_FD_OFFSET;

    // init packet structure
    int packet_len = sizeof(PackedStream) + sizeof(WriteStruct) + count;
    WriteStruct* args;
    PackedStream* msg = prepare_msg(OP_WRITE, (void**)(&args), packet_len);

    // fill in argument struct
    args->fd = fd;
    args->count = count;
    memcpy(args->buf, buf, count);

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    WriteStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    int ret_count = retArgs->ret_count;
    if (ret_count == -1)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return ret_count;
}


off_t lseek(int fd, off_t offset, int whence) {
    // check fd validity
    if (fd < SERVER_FD_OFFSET) {
        return orig_lseek(fd, offset, whence);
    }

    fd -= SERVER_FD_OFFSET;

    // init packet structure
    int packet_len = sizeof(PackedStream) + sizeof(SeekStruct);
    SeekStruct* args;
    PackedStream* msg = prepare_msg(OP_LSEEK, (void**)(&args), packet_len);

    // fill in argument struct
    args->fd = fd;
    args->offset = offset;
    args->whence = whence;

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    SeekStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    off_t ret_off = retArgs->ret_off;
    if (ret_off == -1)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return ret_off;
}


// actual function called by requesting file status is __xstat
int __xstat(int vers, const char* path, struct stat* buf) {
    // init packet structure
    int path_len = strlen(path);
    int packet_len = sizeof(PackedStream) + sizeof(StatStruct) +
                     path_len + 1;
    StatStruct* args;
    PackedStream* msg = prepare_msg(OP_STAT, (void**)(&args), packet_len);

    // fill in argument struct
    args->vers = vers;
    strcpy(args->path, path);

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    StatStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    int ret_val = retArgs->ret_val;
    memcpy(buf, &(retArgs->buf), sizeof(struct stat));
    if (ret_val == -1)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return ret_val;
}


int unlink(const char* pathname) {
    // init packet structure
    int path_len = strlen(pathname);
    int packet_len = sizeof(PackedStream) + sizeof(UnlinkStruct) +
                     path_len + 1;
    UnlinkStruct* args;
    PackedStream* msg = prepare_msg(OP_UNLINK, (void**)(&args), packet_len);

    // fill in argument struct
    strcpy(args->path, pathname);

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    UnlinkStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    int ret_val = retArgs->ret_val;
    if (ret_val == -1)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return ret_val;
}


ssize_t getdirentries(int fd, char* buf, size_t nbytes, off_t* basep) {
    // check fd validity
    if (fd < SERVER_FD_OFFSET) {
        return orig_getdirentries(fd, buf, nbytes, basep);
    }

    fd -= SERVER_FD_OFFSET;

    // init packet structure
    int packet_len = sizeof(PackedStream) + sizeof(DirEntryStruct);
    DirEntryStruct* args;
    PackedStream* msg = prepare_msg(OP_DIR, (void**)(&args), packet_len);

    // fill in argument struct
    args->fd = fd;
    args->bytes = nbytes;
    args->basep = *basep;

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    DirEntryStructRet* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    ssize_t ret_count = retArgs->ret_count;
    *basep = retArgs->basep;
    if (ret_count > 0)
        memcpy(buf, retArgs->buf, ret_count);
    if (ret_count == -1)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return ret_count;
}


struct dirtreenode* getdirtree(const char* path) {
    // init packet structure
    int path_len = strlen(path);
    int packet_len = sizeof(PackedStream) + sizeof(DirTreeStruct) +
                     path_len + 1;
    DirTreeStruct* args;
    PackedStream* msg = prepare_msg(OP_DIRTREE, (void**)(&args), packet_len);

    // fill in argument struct
    strcpy(args->path, path);

    // send streamed arguments and clean up
    send_msg(msg, packet_len, client_sockfd);
    free(msg);

    // receive feedback
    DirTreeStruct* retArgs;
    PackedStream* retMsg = receive_msg(client_sockfd, (void**)(&retArgs));

    // unpack and return to original caller
    struct dirtreenode* root=
        unmarshal_dirtree((void*)retArgs,
                          retMsg->packet_len - sizeof(PackedStream));
    if (root == NULL)
        errno = retMsg->opcode;

    // clean up receive buffer
    free(retMsg);

	return root;
}


/* There is no need for freedirtree to be called remotely
 *
 * Once server marshalled and sent dirtree, it can free the tree immediately
 * At client side, the unmarshalled and reconstructured dirtree would
 *     freed by this call
 */
void freedirtree(struct dirtreenode* dt) {
    orig_freedirtree(dt);
}

/** ! END OF INTERCEPTED CALL ! **/


// This function is automatically called when library is loaded
void _init(void) {
	orig_open = dlsym(RTLD_NEXT, "open");
    orig_close = dlsym(RTLD_NEXT, "close");
    orig_read = dlsym(RTLD_NEXT, "read");
    orig_write = dlsym(RTLD_NEXT, "write");
    orig_lseek = dlsym(RTLD_NEXT, "lseek");
    orig_xstat = dlsym(RTLD_NEXT, "__xstat");
    orig_unlink = dlsym(RTLD_NEXT, "unlink");
    orig_getdirentries = dlsym(RTLD_NEXT, "getdirentries");
    orig_getdirtree = dlsym(RTLD_NEXT, "getdirtree");
    orig_freedirtree = dlsym(RTLD_NEXT, "freedirtree");

    // init connection with server
    init_connection();
}

// Close connection when library unloaded
void _fini(void) {
    orig_close(client_sockfd);
}
