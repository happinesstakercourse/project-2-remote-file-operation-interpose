// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

#include "message_helper.h"

PackedStream* prepare_msg(int fop, void** fop_struct, int total_length) {
    PackedStream* msg = (PackedStream*)malloc(total_length);
    msg->packet_len = total_length;
    msg->opcode = fop;
    *fop_struct = (void*)msg->payload;
    return msg;
}

int send_msg(PackedStream* msg, int length, int sockfd) {
    int rv = send(sockfd, (void*)msg, length, 0);
    if (rv < 0) {
        print_error("Network Error", strerror(errno));
        return -1;
    }
    return 0;
}

PackedStream* receive_msg(int sockfd, void** fop_struct) {
    // receive once and store in buffer to extract total packet length
    char buf[MSG_BLOCK+1];
    int rv = recv(sockfd, buf, MSG_BLOCK, 0);

    // client has close connection, this session ends
    if (rv == 0) {
        return NULL;
    }
    else if (rv < 0) {
        print_error("Network Error", "Receive message fails 1");
        return NULL;
    }

    int received = rv;
    int to_receive = ((PackedStream*)buf)->packet_len;

    // allocate argument struct space
    PackedStream* packed_packet = (PackedStream*)malloc(to_receive);
    memcpy(packed_packet, buf, received);

    // receive all remaining message if available
    while ((received < to_receive) &&
           ((rv = recv(sockfd, buf, MSG_BLOCK, 0)) > 0)) {
        memcpy((char*)packed_packet + received, buf, rv);
        received += rv;
    }

	// either opposite side closed connection too early, or error in network
	if (rv <= 0) {
        print_error("Network Error",
                    "Opposite Close Connection or Receive message fails 2");
        return NULL;
    }

    // assign fop struct location
    *fop_struct = (void*)packed_packet->payload;

    return packed_packet;
}

void print_error(char* type, char* detail) {
    fprintf(stderr, "%s-> %s\n", type, detail);
}

int compute_total_dirtree_length(struct dirtreenode* root) {
    if (root == NULL) {
        return 0;
    }

    int total_length = sizeof(SerialDirTreeNode) + strlen(root->name) + 1;
    if (root->num_subdirs == 0) {
        return total_length;
    } else {
        int index;
        for (index = 0; index < root->num_subdirs; index++) {
            total_length += compute_total_dirtree_length(root->subdirs[index]);
        }
        return total_length;
    }
}

void marshal_dirtree(struct dirtreenode* root, void* buf, int total_length) {

    if (root == NULL) {
        return;
    }

    SerialDirTreeNode* write_ptr = buf;
    SerialDirTreeNode* frontier_ptr = buf;

    // copy root node as starting point
    write_ptr->local_ptr = root;
    write_ptr->num_subdirs = root->num_subdirs;
    strcpy(write_ptr->name, root->name);
    write_ptr = (SerialDirTreeNode*)((void*)(write_ptr + 1) +
                                     strlen(root->name) + 1);

    // execute BFS algo to copy all nodes
    while ((void*)frontier_ptr < buf + total_length) {
        struct dirtreenode* cur_node = frontier_ptr->local_ptr;
        int index;
        for (index = 0; index < cur_node->num_subdirs; index ++) {
            write_ptr->local_ptr = cur_node->subdirs[index];
            write_ptr->num_subdirs = cur_node->subdirs[index]->num_subdirs;
            strcpy(write_ptr->name, cur_node->subdirs[index]->name);
            write_ptr = (SerialDirTreeNode*)
                ((void*)(write_ptr + 1) +
                 strlen(cur_node->subdirs[index]->name) + 1);
        }
        frontier_ptr = (SerialDirTreeNode*)
            ((void*)(frontier_ptr + 1) + strlen(cur_node->name) + 1);
    }
}

struct dirtreenode* unmarshal_dirtree(void* buf, int total_length) {
    if (total_length == 0) {
        return NULL;
    }

    SerialDirTreeNode* parent_ptr = buf;
    SerialDirTreeNode* child_ptr = buf;

    // first allocate root space
    struct dirtreenode* root = (struct dirtreenode*)
        malloc(sizeof(struct dirtreenode));
    // modify reverse pointer field
    child_ptr->local_ptr = root;
    // copy subdir number
    root->num_subdirs = child_ptr->num_subdirs;
    // copy path name
    root->name = (char*)malloc(strlen(child_ptr->name) + 1);
    strcpy(root->name, child_ptr->name);
    // allocate space for subdirs
    if (root->num_subdirs == 0) {
        root->subdirs = NULL;
    } else {
        root->subdirs = (struct dirtreenode**)
            malloc(sizeof(struct dirtreenode*) * root->num_subdirs);
    }
    child_ptr = (SerialDirTreeNode*)
        ((void*)(child_ptr + 1) + strlen(root->name) + 1);

    // execute BFS to reversely deserialize buffer to tree
    while ((void*)parent_ptr < buf + total_length) {
        struct dirtreenode* parent_node = parent_ptr->local_ptr;
        int index;
        for (index = 0; index < parent_node->num_subdirs; index++) {
            struct dirtreenode* cur_node =
                (struct dirtreenode*)malloc(sizeof(struct dirtreenode));
            child_ptr->local_ptr = cur_node;
            cur_node->num_subdirs = child_ptr->num_subdirs;
            cur_node->name = (char*)malloc(strlen(child_ptr->name) + 1);
            strcpy(cur_node->name, child_ptr->name);
            if (cur_node->num_subdirs == 0) {
                cur_node->subdirs = NULL;
            } else {
                cur_node->subdirs = (struct dirtreenode**)
                    malloc(sizeof(struct dirtreenode*) * cur_node->num_subdirs);
            }

            // link parent and child
            parent_node->subdirs[index] = cur_node;
            child_ptr = (SerialDirTreeNode*)
                ((void*)(child_ptr + 1) + strlen(cur_node->name) + 1);
        }
        parent_ptr = (SerialDirTreeNode*)
            ((void*)(parent_ptr + 1) + strlen(parent_node->name) + 1);
    }

    return root;
}
