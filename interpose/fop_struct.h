// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/* ========================================================
 * |             15-640 Distributed System                |
 * |                   Project 1: RPC                     |
 * ========================================================
 *
 * Fop_Struct Header
 *
 * This header file is used to define struct format used by RPC argument
 *     during marshal/unmarshal
 * Both client and server would include this file to retrieve arg fields
 *     or pack to stream
 */


#ifndef FOP_STRUCT_H
#define FOP_STRUCT_H

// maximum network buffer length
#define MSG_BLOCK 512

// file operation op_code
#define OP_OPEN  1
#define OP_CLOSE 2
#define OP_READ  3
#define OP_WRITE 4
#define OP_LSEEK 5
#define OP_STAT  6
#define OP_DIR   7
#define OP_UNLINK 8
#define OP_DIRTREE 9


/** Struct for message packet header send/recv **/
typedef struct PackedStream_t {
    int packet_len;   // used to identify packet length
    int opcode;       // client to server: identity file operation
                      // server to client: error number
    char payload[0];  // actual payload: arguments for RPC
} PackedStream;



/* Open File */
typedef struct OpenStruct_t {
    int flags;
    mode_t m;
    char path[0];
} OpenStruct;

typedef struct OpenStructRet_t {
    int fd;
} OpenStructRet;

/* Close File */
typedef struct CloseStruct_t {
    int fd;
} CloseStruct;

typedef struct CloseStructRet_t {
    int ret_val;
} CloseStructRet;

/* Read from File */
typedef struct ReadStruct_t {
    int fd;
    size_t count;
} ReadStruct;

typedef struct ReadStructRet_t {
    ssize_t ret_count;
    char buf[0];
} ReadStructRet;

/* Write to File */
typedef struct WriteStruct_t {
    int fd;
    size_t count;
    char buf[0];
} WriteStruct;

typedef struct WriteStructRet_t {
    ssize_t ret_count;
} WriteStructRet;

/* Seek in File */
typedef struct SeekStruct_t {
    int fd;
    off_t offset;
    int whence;
} SeekStruct;

typedef struct SeekStructRet_t {
    off_t ret_off;
} SeekStructRet;

/* List file status */
typedef struct StatStruct_t {
    int vers;
    char path[0];
} StatStruct;

typedef struct StatStructRet_t {
    int ret_val;
    struct stat buf;
} StatStructRet;

/* List Dir Entries */
typedef struct DirEntryStruct_t {
    int fd;
    size_t bytes;
    off_t basep;
} DirEntryStruct;

typedef struct DirEntryStructRet_t {
    ssize_t ret_count;
    off_t basep;
    char buf[0];
} DirEntryStructRet;

/* Unlink Files */
typedef struct UnlinkStruct_t {
    char path[0];
} UnlinkStruct;

typedef struct UnlinkStructRet_t {
    int ret_val;
} UnlinkStructRet;

/* List Dir Tree */
typedef struct DirTreeStruct_t {
    char path[0];
} DirTreeStruct;

typedef struct SerialDirTreeNode_t {
    struct dirtreenode* local_ptr;
    int num_subdirs;
    char name[0];
} SerialDirTreeNode;


#endif
