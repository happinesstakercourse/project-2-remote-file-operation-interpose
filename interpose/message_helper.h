// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/* ========================================================
 * |             15-640 Distributed System                |
 * |                   Project 1: RPC                     |
 * ========================================================
 *
 * Message_Helper Module
 *
 * Message Helper Module is designed to generalize and extract common
 *     procedures in marshal/unmarshal, TCP send/receive
 * Both client and server would include this module, to make the code
 *     in their part more focus on file operations themselves
 */

#ifndef MESSAGE_HELPER_H
#define MESSAGE_HELPER_H

// linux libs
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

// error handling
#include <string.h>
#include <errno.h>

// network library
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>

// file operation library
#include <dlfcn.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/types.h>
#include <sys/stat.h>

// user defined fop types
#include "fop_struct.h"
#include "dirtree.h"


/* @summary: initialize message packet structure
 * @argument:
 *      <In>[int]fop: file operation to carry out (specified in fop_struct.h)
 *      <Out>[void**]fop_struct: reference to fop structure pointer, used to
 *                               pass out location for initialized fop
 *                               struct in message
 *      <In>[int]fop_struct_len: used to calculate whole packet length
 *      <Out>[PackedStream*]: return the pointer to whole message packet
 */
PackedStream* prepare_msg(int fop, void** fop_struct, int fop_struct_len);

/* @summary: send out marshalled message buffer by TCP
 * @detail: send_msg would only try one send currently, as it is working fine
 *          without looping and making sure specified length is sent
 */
int send_msg(PackedStream* msg, int length, int sockfd);

/* @summary: receive message from socket and return packet pointer
 *           and file operation pointer
 * @detail: receive_msg would call recv to store first part of packet in static
 *          buffer and extract whole packet length. then a loop of recv are
 *          called to receive the whole packet.
 */
PackedStream* receive_msg(int sockfd, void** fop_struct);

/* @summary: helper function to print out error to stderr
 * @detail: this function would not cause exit, as the server may want to print
 *          error for this client and keep running. caller should be responsible
 *          for handling various errors.
 */
void print_error(char* type, char* detail);

/* @summary: compute total length to marshal dir node tree to serialized buffer
 * @detail: used before actually filling nodes onto buffer
 *          use DFS to compute length
 */
int compute_total_dirtree_length(struct dirtreenode* root);

/* @summary: given the root of dirtree, marshal the tree into serialized buffer
 * @detail: the order of filling nodes follow BFS
 */
void marshal_dirtree(struct dirtreenode* root, void* buf, int total_length);

/* @summary: at receiving side, use serialized buffer to reconstruct dirtree
 * @detail: use the same BFS algorithm as marshal_dirtree to reversely construct
 *          the tree and allocate memory for all tree nodes
 */
struct dirtreenode* unmarshal_dirtree(void* buf, int total_length);

#endif
