// Author: Jiajie YANG <jiajiey@andrew.cmu.edu>

/* ========================================================
 * |             15-640 Distributed System                |
 * |                   Project 1: RPC                     |
 * ========================================================
 *
 * RPC Server
 *
 * This server would interpret message from client and execute file operations
 *     on their behalves
 * Marshal/Unmarshal and send/recv procedures are similar to the client side
 */

#include "message_helper.h"


/** ! START OF RPC EXECUTION CALL ! **/

int execute_open(OpenStruct* args, int sockfd){
    // unpack arguments
    int flags = args->flags;
    mode_t m = args->m;
    char* path = args->path;

    // execute RPC
    int fd = open(path, flags, m);

    // init return packet structure
    int ret_length = sizeof(PackedStream) + sizeof(OpenStructRet);
    OpenStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->fd = fd;

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_close(CloseStruct* args, int sockfd){
    // unpack arguments
    int fd = args->fd;

    // execute RPC
    int ret_val = close(fd);

    // pack return arguments
    int ret_length = sizeof(PackedStream) + sizeof(CloseStructRet);
    CloseStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_val = ret_val;

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_write(WriteStruct* args, int sockfd){
    // unpack arguments
    int fd = args->fd;
    size_t count = args->count;
    void* buf = args->buf;

    // execute RPC
    int ret_count = write(fd, buf, count);

    // pack return arguments
    int ret_length = sizeof(PackedStream) + sizeof(WriteStructRet);
    WriteStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_count = ret_count;

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_read(ReadStruct* args, int sockfd) {
    // unpack arguments
    int fd = args->fd;
    size_t count = args->count;
    void* buf;
    if (count == 0) {
        buf = NULL;
    } else {
        buf = malloc(count);
        memset(buf, 0, count);
    }

    // execute RPC
    ssize_t ret_count = read(fd, buf, count);

    // pack return arguments
    int ret_length;
    if (ret_count >= 0)
        ret_length = sizeof(PackedStream) + sizeof(ReadStructRet) + ret_count;
    else
        ret_length = sizeof(PackedStream) + sizeof(ReadStructRet);

    ReadStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_count = ret_count;
    // integer overflow vulnerability caution !!
    if (ret_count >= 0)
        memcpy(retArgs->buf, buf, ret_count);
    free(buf);

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;

}

int execute_lseek(SeekStruct* args, int sockfd){
    // unpack arguments
    int fd = args->fd;
    off_t offset = args->offset;
    int whence = args->whence;

    // execute RPC
    int ret_off = lseek(fd, offset, whence);

    // pack return arguments
    int ret_length = sizeof(PackedStream) + sizeof(SeekStructRet);
    SeekStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_off = ret_off;

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_stat(StatStruct* args, int sockfd){
    // unpack arguments
    int vers = args->vers;
    char* path = args->path;
    struct stat* buf = malloc(sizeof(struct stat));

    // execute RPC
    int ret_val = __xstat(vers, path, buf);

    // pack return arguments
    int ret_length = sizeof(PackedStream) + sizeof(StatStructRet);
    StatStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_val = ret_val;
    memcpy(&(retArgs->buf), buf, sizeof(struct stat));
    free(buf);

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_dir(DirEntryStruct* args, int sockfd){
    // unpack arguments
    int fd = args->fd;
    size_t bytes = args->bytes;
    off_t base_off = args->basep;
    off_t* basep = &base_off;
    void* buf;
    if (bytes == 0) {
        buf = NULL;
    } else {
        buf = malloc(bytes);
    }

    // execute RPC
    ssize_t ret_count = getdirentries(fd, buf, bytes, basep);

    // pack return arguments
    int ret_length;
    if (ret_count > 0) {
        ret_length = sizeof(PackedStream) +
            sizeof(DirEntryStructRet) + ret_count;
    }
    else {
        ret_length = sizeof(PackedStream) + sizeof(DirEntryStructRet);
    }

    DirEntryStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_count = ret_count;
    retArgs->basep = base_off;
    if (ret_count >= 0)
        memcpy(retArgs->buf, buf, ret_count);
    free(buf);

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_unlink(UnlinkStruct* args, int sockfd){
    // unpack arguments
    char* path = args->path;

    // execute RPC
    int ret_val = unlink(path);

    // init return packet structure
    int ret_length = sizeof(PackedStream) + sizeof(UnlinkStructRet);
    UnlinkStructRet* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    retArgs->ret_val = ret_val;

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);

    return 0;
}

int execute_dirtree(DirTreeStruct* args, int sockfd){
    // unpack arguments
    char* path = args->path;

    // execute RPC
    struct dirtreenode* root = getdirtree(path);

    // init return packet structure
    int buffer_length = compute_total_dirtree_length(root);
    int ret_length = sizeof(PackedStream) +
        sizeof(DirTreeStruct) + buffer_length;
    DirTreeStruct* retArgs;
    PackedStream* retMsg = prepare_msg(errno, (void**)(&retArgs), ret_length);

    // fill in return arguments
    marshal_dirtree(root, (void*)retArgs, buffer_length);

    // send back return value
    send_msg(retMsg, ret_length, sockfd);

    // cleanup
    free(retMsg);
    freedirtree(root);

    return 0;
}

/** ! END OF RPC EXECUTION CALL ! **/


/* Dispatcher for RPC according to opcode */
int handle_rpc(int sessfd) {
    PackedStream* cmd;
    void* args;

    // each loop is complete packet for one file operation
    while ((cmd = receive_msg(sessfd, &args))) {
        // execute RPC according to opcode
        switch(cmd->opcode){
        case OP_OPEN:
            execute_open((OpenStruct*)args, sessfd);
            break;
        case OP_CLOSE:
            execute_close((CloseStruct*)args, sessfd);
            break;
        case OP_READ:
            execute_read((ReadStruct*)args, sessfd);
            break;
        case OP_WRITE:
            execute_write((WriteStruct*)args, sessfd);
            break;
        case OP_LSEEK:
            execute_lseek((SeekStruct*)args, sessfd);
            break;
        case OP_STAT:
            execute_stat((StatStruct*)args, sessfd);
            break;
        case OP_DIR:
            execute_dir((DirEntryStruct*)args, sessfd);
            break;
        case OP_UNLINK:
            execute_unlink((UnlinkStruct*)args, sessfd);
            break;
        case OP_DIRTREE:
            execute_dirtree((DirTreeStruct*)args, sessfd);
            break;
        default:
            print_error("Command Error", "Unrecognized Opcode Received");
        }

        free(cmd);
    }

    close(sessfd);
    exit(0);
}



int main(int argc, char**argv) {
	char *serverport = NULL;
	unsigned short port;
	int sockfd, sessfd, rv;
	struct sockaddr_in srv, cli;
	socklen_t sa_size;

	// Get environment variable indicating the port of the server
	serverport = getenv("serverport15440");
	if (serverport) {
        port = (unsigned short)atoi(serverport);
    } else {
        port = 15987;
    }

	// Create socket
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
        print_error("Network Error", "Cannot create socket");
        return -1;
    }

	// setup address structure to indicate server port
	memset(&srv, 0, sizeof(srv));			    // clear it first
	srv.sin_family = AF_INET;			        // IP family
	srv.sin_addr.s_addr = htonl(INADDR_ANY);	// don't care IP address
	srv.sin_port = htons(port);			        // server port

	// bind to our port
	rv = bind(sockfd, (struct sockaddr*)&srv, sizeof(struct sockaddr));
	if (rv < 0) {
        print_error("Network Error", "Cannot bind listen port");
        return -1;
    }

	// start listening for connections
	rv = listen(sockfd, 5);
	if (rv < 0) {
        print_error("Network Error", "Cannot setup listen queue");
    }

    while (1) {
		// wait for next client, get session socket
		sa_size = sizeof(struct sockaddr_in);
		sessfd = accept(sockfd, (struct sockaddr *)&cli, &sa_size);
		if (sessfd<0) {
            print_error("Network Error", "Cannot accept client connection");
            continue;
        }

        // fork process and let child serve client
        pid_t pid = fork();
        if (pid < 0) {
            print_error("Local Error", "Cannot fork child!");
        } else if (pid == 0) {
            close(sockfd);
            handle_rpc(sessfd);
        } else {
            close(sessfd);
        }
	}

    // never executed here
	close(sockfd);

	return 0;
}
